import allure
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


def test_check_data_on_page():
    """
    1. открыть lamoda.ru
    2. выбрать регион новосибирск
    3. открыть страницу подробнее о доставке
    4. изменить город доставки на г Москва
    5. проверить что присутсвтуют 6 типов доставки, проверить их цены, и сроки доставки
    """

    initial_city = 'Новосибирск'
    change_city = 'Москва'

    expected_shipment_data = [
        {
            'name': 'Без примерки',
            'price': 'Бесплатно',
            'shipment_term': 'На следующий день',
            'fitting': 'Отсутствует',
            'payment': 'Наличными или банковской картой'
        },
        {
            'name': 'С примеркой',
            'price': 'Бесплатно',
            'shipment_term': 'На следующий день',
            'fitting': 'Время на примерку одного заказа - 15 минут.\nВозможна при заказе до 10 товаров.',
            'payment': 'Наличными или банковской картой'
        },
        {
            'name': 'Сегодня, с примеркой',
            'price': '299 руб.',
            'shipment_term': 'В день заказа',
            'fitting': 'Время на примерку одного заказа - 15 минут.\nВозможна при заказе до 10 товаров.',
            'payment': 'Наличными или банковской картой'
        },
        {
            'name': 'В течение выбранного часа, с примеркой',
            'price': '350 руб.',
            'shipment_term': 'На следующий день',
            'fitting': 'Время на примерку одного заказа - 15 минут.\nВозможна при заказе до 10 товаров.',
            'payment': 'Наличными или банковской картой'
        },
        {
            'name': 'В течение выбранных 15 минут, с примеркой',
            'price': '800 руб.',
            'shipment_term': 'На следующий день',
            'fitting': 'Время на примерку одного заказа - 15 минут.\nВозможна при заказе до 10 товаров.',
            'payment': 'Наличными или банковской картой'
        },
        {
            'name': 'В течение выбранного часа, с примеркой',
            'price': '350 руб.',
            'shipment_term': 'На следующий день',
            'fitting': 'Время на примерку одного заказа - 15 минут.\nВозможна при заказе до 10 товаров.',
            'payment': 'Наличными или банковской картой'
        }
    ]

    driver = WebDriver(executable_path='D://selenium//chromedriver.exe')
    driver.implicitly_wait(3)
    with allure.step('Открываем https://www.lamoda.ru/'):
        driver.get('https://www.lamoda.ru/')

    with allure.step('Открываем меню выбора региона'):
        region_picked = driver.find_element_by_xpath('//span[@class="header__top-description-region"]')
        region_picked.click()

    with allure.step(f'Вводим {initial_city} в поле выбра региона'):
        input_field = driver.find_element_by_xpath('//form[@class="geo"]//input')
        input_field.send_keys(initial_city)

    with allure.step('Выбираем подходящий саджест'):
        suggests = driver.find_elements_by_xpath('//ul[@class="geo__suggest"]//li[contains(@class,"suggest__item")]')
        for suggest in suggests:
            if initial_city in suggest.text:
                suggest.click()
                break
        else:
            raise AssertionError(f'Отсутсвтует город {initial_city}')
        region_save_btn = driver.find_element_by_xpath('//button[contains(@class,"geo__button-save")]')
        region_save_btn.click()
    with allure.step('Переходим на страницу деталей доставки'):
        shipment_details_btn = driver.find_element_by_xpath('//a[@class="header__top-item_yellow"]')
        shipment_details_btn.click()
        driver.switch_to.window(driver.window_handles[1])

    with allure.step('Меняем город доставки'):
        delivery_city_input = driver.find_element_by_xpath('//dd[contains(@class,"delivery__city-name")]//input')
        delivery_city_input.clear()
        delivery_city_input.send_keys(change_city)
        delivery_suggests = driver.find_elements_by_xpath('//ul[@class="geo__suggest"]//li[contains(@class,"suggest__item")]')
        for delivery_suggest in delivery_suggests:
            if change_city in delivery_suggest.text:
                delivery_suggest.click()
                break
        else:
            raise AssertionError(f'Отсутсвтует город {change_city}')

    with allure.step('Ожидаем применения выбранного города'):
        WebDriverWait(driver, 10).until(
            expected_conditions.invisibility_of_element_located(
                (By.XPATH, '//dd[contains(@class,"delivery__city-name")]//input[contains(@class,"button_inprogress")]')
            )
        )
    delivery_types = driver.find_elements_by_xpath('//div[@class="delivery_col"]')
    for delivery_type, expected_data in zip(delivery_types, expected_shipment_data):
        d_type, term, fitting, payment = delivery_type.find_elements_by_xpath('.//div[@class="delivery_row"]')
        name = d_type.find_element_by_xpath('.//div[@class="delivery_item-name_type"]')
        price = d_type.find_element_by_xpath('.//div[@class="delivery_item-green"]')
        assert term.text == expected_data['shipment_term']
        assert fitting.text == expected_data['fitting']
        assert payment.text == expected_data['payment']
        assert name.text == expected_data['name']
        assert price.text == expected_data['price']
    driver.close()
